<h1 align="center">API Sicredi - Simulações de Crédito</h1>

<div align="center">

![Licence](https://img.shields.io/static/v1?label=Status&message=CONCLUÍDO&color=green&style=for-the-badge)

![Licence](https://img.shields.io/static/v1?label=License&message=MIT&color=blue&style=for-the-badge)
</div>

___
## 📍 Descrição do Projeto
O projeto foi construído visando elaborar e executar um planejamento de automação de testes para a <a href="https://github.com/desafios-qa-automacao/desafio-sicredi">API do Sicredi</a>, a qual possui o objetivo de simular a tomada de empréstimos. O projeto foi elaborado como requisito para a conclusão do programa de bolsas da empresa Compass.UOL, referente a automação de teste com Java, sendo denominado como Challenge Final.

___
## 📁 Organização do Projeto
O projeto está organizado na seguinte estrutura de pastas: 
- 📁[api-sicredi-master](https://gitlab.com/Ingridy.aless/challenge-final-compass/-/tree/main/api-sicredi-master) : Código para execução das funcionalidades da API Sicredi e detalhamento do projeto base da API.  
- 📁[api-sicredi-planejamento-teste](https://gitlab.com/Ingridy.aless/challenge-final-compass/-/tree/main/api-sicredi-planejamento-teste) : Documentação do plano de teste e mapa mental para à API Sicredi. 

- 📁[api-sicredi-automacao-teste](https://gitlab.com/Ingridy.aless/challenge-final-compass/-/tree/develop/api-sicredi-automacao-teste/Sicredi) : Projeto e código para execução dos testes automatizados. 
___
## 🖥️ Funcionalidades da API
- ✔️ Consultar se um CPF possuir restrições ou não;
- ✔️ Cadastrar de simulação de crédito;
- ✔️ Consultar todas as simulações de créditos cadastras;
- ✔️ Consultar simulações de crédito pelo CPF;
- ✔️ Alterar simulação de crédito pelo CPF;
- ✔️ Deletar simulação de crédito pelo ID da simulação.
___
## ⚙️ Pré-requisitos e como rodar a aplicação/testes

### 📋 Pré-requisitos para rodar a aplicação
- É preciso instalar as seguintes aplicações: 

    - <a href="https://www.oracle.com/java/technologies/downloads/">Java 8+ JDK</a>
    - <a href="https://maven.apache.org/download.cgi">Maven</a>

___
### 🔧 Etapas para rodar a aplicação
1. Clonar o repositório através do seu prompt de comando/terminal/console executando o comando:
```
git clone https://gitlab.com/Ingridy.aless/projeto-compass.git
```
2. Na pasta [api-sicredi-master](https://gitlab.com/Ingridy.aless/challenge-final-compass/-/tree/main/api-sicredi-master/prova-tecnica-api), na raiz do projeto, através do seu prompt de comando/terminal/console executar o comando:
```
mvn clean spring-boot:run
```
3. Após, a aplicação estará disponível na URL: http://localhost:8080. É importante não fechar seu prompt de comando/terminal/console durante a execução da API. 

4. A documentação da API pode ser acessa no endereço: http://localhost:8080/swagger-ui.html.

___
### 📋 Pré-requisitos para rodar os testes
- É preciso instalar as seguintes aplicações: 

    - <a href="https://www.oracle.com/java/technologies/downloads/">Java 8+ JDK</a>
    - <a href="https://maven.apache.org/download.cgi">Maven</a>
    - <a href="https://projectlombok.org/">Lombok</a>

___
### 🔧 Etapas para executar os testes automatizados
1. Na pasta [api-sicredi-automacao-teste](https://gitlab.com/Ingridy.aless/challenge-final-compass/-/tree/develop/api-sicredi-automacao-teste/Sicredi), na raiz do projeto, abrir outra janela do prompt de comando/terminal/console e executar o comando:
```
mvn clean test
```
2. Ao fim é retornado o número de teste executados com sucesso e o tempo de execução.

3. Para gerar os reports com a ferramenta Allure, deve-se executar no mesmo prompt de comando/terminal/console anterior (item 1) os seguintes comandos:
 ```
mvn allure:report
```
4. Após a geração dos reports, deve-se executar no mesmo prompt de comando/terminal/console anterior, o comando:
```
mvn allure:serve
```
5. Por fim, obtém uma página web com o servidor do Alurre, mostrando os reports da execução dos testes.

___
## ✒️ Autores e Colaboradores

<table>
  <tr>
    <td align="center">
      <a href="#">
        <img src="https://gitlab.com/uploads/-/system/user/avatar/14635018/avatar.png?width=400" width="100px;" alt="Foto do Ingridy no GitHub"/><br>
        <sub>
          <b>Ingridy Alessandretti</b>
        </sub>
      </a>
    </td>
  </tr>
</table>

___
## 📄 Licença
Este projeto está licenciado sob: [MIT LICENSE](https://gitlab.com/Ingridy.aless/challenge-final-compass/-/blob/main/LICENSE).

___
## 🎁 Agradecimentos
Agradecimentos aos colegas do programa de bolsa, a equipe do Academy e a de QA da Compass.UOL e ao Scrum Master Rafael Vescio. 