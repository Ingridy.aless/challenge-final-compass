<h1 align="center">Plano de Teste API Sicredi</h1>

___
# 1. Objetivo e Escopo do Plano de Teste
O plano de teste é baseado na <a href="https://github.com/desafios-qa-automacao/desafio-sicredi">API do Sicredi</a>, em que o objetivo será permitir que clientes do Sicredi possam consultar se possuem restrições em seu CPF, além de realizar simulações de empréstimos. 
O escopo do plano de teste é baseado nas User Stories 01 e 02 referentes as rotas de Restrições e Simulações, respectivamente. 
___
# 2. Stakeholders
- Equipe de testes: Ingridy Alessandretti.
- Público-alvo: clientes da instituição financeira cooperativa Sicredi.
___
# 3. Estratégia de Teste
A estratégia de teste deste plano é divida em 2 rotas, contemplando as regras de negócios, casos de teste, testes candidatos à automação, previsão de cobertura de testes para cada rota, além das ferramentas e tecnologias utilizadas durante a execução dos testes.
___

## 3.1 Regras de Negócio
A seguir temos as regras de negócio de cada rota da API Sicredi, descritas conforme a <a href="https://github.com/desafios-qa-automacao/desafio-sicredi/blob/master/Orienta%C3%A7%C3%B5es%20para%20execu%C3%A7%C3%A3o%20do%20Desafio_v1.pdf">documentação da API</a>.
___
### 3.1.1 [API] Restrições - User Story 01  
- Ao verificar um CPF com restrição, uma mensagem deve ser informada ao cliente;
- Ao verificar um CPF sem restrição, nenhuma mensagem deve ser informada ao cliente.
___
### 3.1.2 [API] Simulações - User Story 02
- Os campos CPF, nome, e-mail, valor, parcela e seguro possuem preenchimento obrigatório nos métodos PUT e POST;
- Não deve ser possível cadastrar e alterar simulações com CPF no formato 999.999.999-99;
- Não deve ser possível cadastrar e alterar simulações com e-mail em um formato inválido;
- Não deve ser possível cadastrar e alterar simulações com um valor menor que R$1.000 ou maior que R$40.000;
- Não deve ser possível cadastrar e alterar simulações com um número de parcelas menor que 2 ou maior que 48 vezes;
- Clientes com uma simulação já cadastrada em seu CPF não devem conseguir realizar outras simulações;
- Não deve ser possível alterar o campo CPF de uma simulação para outro CPF já cadastrado em outra simulação;
- Deve ser possível alterar qualquer atributo da simulação;
- Não deve ser possível alterar e listar simulações não cadastradas e vinculadas a um CPF.
___
## 3.2 Casos de Teste
Os casos de teste para cada rota da API constam nos itens abaixo, sendo estes identificados conforme a sigla CT00, em ordem numérica crescente. 
___
### 3.2.1 [API] Restrições - User Story 01  

<tabLe>
    <tr  align="center">
        <th>ID</th>
        <th>Caso de Teste</th>
    </tr>
    <tr  align="center">
        <td>CT 01</td>
        <td>Consultar um CPF sem restrição</td>
    </tr>
    <tr  align="center">
        <td>CT 02</td>
        <td>Consultar um CPF com restrição</td>
    </tr>
    <tr  align="center">
        <td>CT 03</td>
        <td>Consultar um CPF com quantidade de dígitos superior e inferior a 11</td>
    </tr>
    <tr  align="center">
        <td>CT 04</td>
        <td>Consultar um CPF inválido conforme as regras da receita federal</td>
    </tr>
    <tr  align="center">
        <td>CT 05</td>
        <td>Consultar um CPF inserindo caracteres especiais e letras</td>
    </tr>
    <tr  align="center">
        <td>CT 06</td>
        <td>Consultar um CPF no formato 999.999.999-99</td>
    </tr>
</tabLe>

___
### 3.2.2 [API] Simulações - User Story 02  
<tabLe>
    <tr  align="center">
        <th>ID</th>
        <th>Caso de Teste</th>
    </tr>
    <tr  align="center">
        <td>CT 01</td>
        <td>Listar todas as simulações existentes</td>
    </tr>
    <tr  align="center">
        <td>CT 02</td>
        <td>Listar uma simulação pelo CPF</td>
    </tr>
    <tr  align="center">
        <td>CT 03</td>
        <td>Listar uma simulação por um CPF inexistente</td>
    </tr>
    <tr  align="center">
        <td>CT 04</td>
        <td>Criar uma simulação preenchendo todos os campos corretamente</td>
    </tr>
     <tr  align="center">
        <td>CT 05</td>
        <td>Criar uma simulação com CPF já cadastrado em outra simulação</td>
    </tr>
     <tr  align="center">
        <td>CT 06</td>
        <td>Criar uma simulação com campos obrigatórios de nome, CPF e e-mail como string em branco</td>
    </tr>
     <tr  align="center">
        <td>CT 07</td>
        <td>Criar uma simulação com CPF no formato 999.999.999-99</td>
    </tr>
     <tr  align="center">
        <td>CT 08</td>
        <td>Criar uma simulação com e-mail em um formato inválido</td>
    </tr>
    <tr  align="center">
        <td>CT 09</td>
        <td>Criar uma simulação com o valor menor que R$ 1.000,00</td>
    </tr>
    <tr  align="center">
        <td>CT 10</td>
        <td>Criar uma simulação com o valor  maior que R$ 40.000,00</td>
    </tr>
    <tr  align="center">
        <td>CT 11</td>
        <td>Criar uma simulação com o número de parcelas inferior a 2 vezes</td>
    </tr>
    <tr  align="center">
        <td>CT 12</td>
        <td>Criar uma simulação com o número de parcelas superior a 48 vezes</td>
    </tr>
    <tr  align="center">
        <td>CT 13</td>
        <td>Criar uma simulação com CPF com quantidade de dígitos superior e inferior a 11</td>
    </tr>
    <tr  align="center">
        <td>CT 14</td>
        <td>Criar uma simulação com CPF com caracteres especiais e letras</td>
    </tr>
    <tr  align="center">
        <td>CT 15</td>
        <td>Criar uma simulação com CPF inválido conforme regras da receita federal</td>
    </tr>
    <tr  align="center">
        <td>CT 16</td>
        <td>Criar uma simulação com campos obrigatórios nulos</td>
    </tr>
    <tr  align="center">
        <td>CT 17</td>
        <td>Criar uma simulação trocando campos tipo string (nome, CPF e e-mail) para integer e campos numéricos (valor, parcela e seguro) para tipo string</td>
    </tr>
    <tr  align="center">
        <td>CT 18</td>
        <td>Deletar uma simulação existente pelo id</td>
    </tr>
    <tr  align="center">
        <td>CT 19</td>
        <td>Deletar uma simulação inexistente pelo id</td>
    </tr>
    <tr  align="center">
        <td>CT 20</td>
        <td>Atualizar uma simulação existente pelo CPF</td>
    </tr>
    <tr  align="center">
        <td>CT 21</td>
        <td>Atualizar uma simulação inexistente pelo CPF</td>
    </tr>
    <tr  align="center">
        <td>CT 22</td>
        <td>Atualizar uma simulação com CPF já cadastrado em outra simulação</td>
    </tr>
    <tr  align="center">
        <td>CT 23</td>
        <td>Atualizar uma simulação com campos obrigatórios de nome, CPF e e-mail como string em branco</td>
    </tr>
    <tr  align="center">
        <td>CT 24</td>
        <td>Atualizar uma simulação com CPF no formato 999.999.999-99</td>
    </tr>
    <tr  align="center">
        <td>CT 25</td>
        <td>Atualizar uma simulação com e-mail em um formato inválido</td>
    </tr>
    <tr  align="center">
        <td>CT 26</td>
        <td>Atualizar simulação com o valor  menor que R$ 1.000,00</td>
    </tr>
    <tr  align="center">
        <td>CT 27</td>
        <td>Atualizar simulação com o valor  maior que R$ 40.000,00</td>
    </tr>
    <tr  align="center">
        <td>CT 28</td>
        <td>Atualizar uma simulação com o número de parcelas inferior a 2 vezes</td>
    </tr>
    <tr  align="center">
        <td>CT 29</td>
        <td>Atualizar uma simulação com o número de parcelas superior a 48 vezes</td>
    </tr>
    <tr  align="center">
        <td>CT 30</td>
        <td>Atualizar uma simulação com CPF com quantidade de dígitos superior e inferior a 11</td>
    </tr>
    <tr  align="center">
        <td>CT 31</td>
        <td>Atualizar uma simulação com CPF com caracteres especiais e letras</td>
    </tr>
    <tr  align="center">
        <td>CT 32</td>
        <td>Atualizar uma simulação com CPF inválido conforme regras da receita federal</td>
    </tr>
    <tr  align="center">
        <td>CT 33</td>
        <td>Atualizar uma simulação com campos obrigatórios nulos</td>
    </tr>
    <tr  align="center">
        <td>CT 34</td>
        <td>Atualizar uma simulação trocando campos tipo string (nome, cpf e email) para integer e campos numéricos (valor, parcela e seguro) para tipo string</td>
    </tr>
</tabLe>

___
### 3.2 Teste Candidatos à Automação
Os testes candidatos à automação para cada rota da API constam na tabela abaixo:
<tabLe>
    <tr  align="center">
        <th>[API] GET - Restrições</th>
        <th>[API] GET - Simulações</th>
        <th>[API] POST - Simulações</th>
        <th>[API] DELETE - Simulações</th>
        <th>[API] PUT - Simulações</th>
    </tr>
    <tr  align="center">
        <td>CT01</td>
        <td>CT01</td>
        <td>CT04</td>
        <td>CT18</td>
        <td>CT20</td>
    </tr>
    <tr  align="center">
        <td>CT02</td>
        <td>CT02</td>
        <td>CT05</td>
        <td>CT19</td>
        <td>CT21</td>
    </tr>
    <tr  align="center">
        <td></td>
        <td>CT03</td>
        <td>CT08</td>
        <td></td>
        <td>CT22</td>
    </tr>
    <tr  align="center">
        <td></td>
        <td></td>
        <td>CT09</td>
        <td></td>
        <td>CT25</td>
    </tr>
    <tr  align="center">
        <td></td>
        <td></td>
        <td>CT10</td>
        <td></td>
        <td>CT26</td>
    </tr>
    <tr  align="center">
        <td></td>
        <td></td>
        <td>CT11</td>
        <td></td>
        <td>CT27</td>
    </tr>
    <tr  align="center">
        <td></td>
        <td></td>
        <td>CT12</td>
        <td></td>
        <td>CT28</td>
    </tr>
    <tr  align="center">
        <td></td>
        <td></td>
        <td>CT16</td>
        <td></td>
        <td>CT29</td>
    </tr>
    <tr  align="center">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>CT33</td>
    </tr>
</table>

___
### 3.3 Previsão de Cobertura de Testes
<tabLe>
    <tr  align="center">
        <th>Critério de Cobertura</th>
        <th>Resultado de Cobertura na API</th>
        <th>Resultado de Cobertura [API] Restrições</th>
        <th>Resultado de Cobertura [API] Simulações</th>
    </tr>
    <tr  align="center">
        <td>Path Coverage (Input)</td>
        <td>100%</td>
        <td>100%</td>
        <td>100%</td>
    </tr>
    <tr  align="center">
        <td>Operator Coverage (Input)</td>
        <td>100%</td>
        <td>100%</td>
        <td>100%</td>
    </tr>
    <tr  align="center">
        <td>Status Code Coverage (Output)</td>
        <td>100%</td>
        <td>100%</td>
        <td>100%</td>
    </tr>
</table>

___
### 3.4 Ferramentas e Tecnologias
- Gitlab;
- Swagger API;
- Jira;
- XMind 22.11;
- Postman 10.16.0;
- Eclipse IDE 2023-06 (4.28.0);
- Java JDK 20.0.1;
- Apache Maven 3.9.2;
- RestAssured 5.3.1;
- Project Lombok 1.18.28;
- JavaFaker 1.0.2;
- JUnit Jupiter 5.9.3;
- Json Schema Validator 5.3.1;
- Jackson 2.15.2;
- Wiremock 2.33.2;
- Allure 2.18.1;
- API Sicredi v2.1.5.
___
## 4. Ambiente Teste 
Os testes serão realizados em Notebook Dell Inspirion 14 i7 8GB de RAM e API é executada em um servidor local na porta 8080.
___
## 5. Cronograma de Atividades
<table>
    <tr  align="center">
        <th>Atividade</th>
        <th>Estimativa de Data de Início</th>
        <th>Estimativa de Data de Conclusão</th>
    </tr>
    <tr  align="center">
        <td>Elaboração do Plano de Testes</td>
        <td>01/08/2023</td>
        <td>03/08/2023</td>
    </tr>
    <tr  align="center">
        <td>Elaboração do Projeto de Testes no Jira</td>
        <td>03/08/2023</td>
        <td>04/08/2023</td>
    </tr>
    <tr  align="center">
        <td>Elaboração e Execução dos Testes Automatizados</td>
        <td>07/08/2023</td>
        <td>09/08/2023</td>
    </tr>
    <tr  align="center">
        <td>Relatório de Bugs e Melhorias</td>
        <td>10/08/2023</td>
        <td>10/08/2023</td>
    </tr>
    <tr  align="center">
        <td>Apresentação do Plano e dos Resultados</td>
        <td>11/08/2023</td>
        <td>11/08/2023</td>
    </tr>
</table>

___
## 6. Documentos Complementares
<div align="center">
<h4>Mapa Mental API Sicredi</h4>

![Mapa Mental](/api-sicredi-planejamento-teste/mapa_mental_api_sicredi.png)
</div>

<div align="center">
<h4>Projeto Jira API Sicredi</h4>
<a href="https://ingridyalessandretti7.atlassian.net/jira/software/c/projects/AS/boards/4" target="_blank"><img src="https://img.shields.io/badge/Jira-0052CC?style=for-the-badge&logo=Jira&logoColor=white" target="_blank"></a>
</div>