package services;

import java.text.MessageFormat;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class BaseRest {

	public static Response get(String endpoint, String cpf) {
		String path = MessageFormat.format(endpoint + "/{0}", cpf);
		return RestAssured.get(path).then().extract().response();
	}
	
	public static Response delete(String endpoint, Integer id) {
		String path = MessageFormat.format(endpoint + "/{0}", id);
		return RestAssured.delete(path).then().extract().response();
	}
	
	public static Response post(String endpoint, Object payload) {
		return RestAssured.given().body(payload).post(endpoint).then().extract().response();
	}
	
	public static Response put(String endpoint, String cpf, Object payload) {
		String path = MessageFormat.format(endpoint + "/{0}", cpf);
		return RestAssured.given().body(payload).put(path).then().extract().response();
	}
}