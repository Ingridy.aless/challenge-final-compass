package model;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Simulacao {
	
	private String nome;
	private String cpf;
	private String email;
	private Number valor;
	private Integer parcelas;
	private Boolean seguro;
}