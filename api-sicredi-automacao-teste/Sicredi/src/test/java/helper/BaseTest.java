package helper;

import org.junit.jupiter.api.*;
import io.restassured.RestAssured;
import io.restassured.builder.*;
import io.restassured.http.ContentType;

public class BaseTest {

	@BeforeAll
	public static void setup() {
		
		RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
		reqBuilder.setBaseUri(EnvConfig.getProperty("url", ""));
		reqBuilder.setContentType(ContentType.JSON); 
		RestAssured.requestSpecification = reqBuilder.build();
		
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}
}