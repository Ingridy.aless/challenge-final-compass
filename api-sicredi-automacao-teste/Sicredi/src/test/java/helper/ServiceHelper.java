package helper;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import java.text.MessageFormat;
import io.restassured.response.Response;

public class ServiceHelper {
	
	public static void validarJsonSchema(Response response, String endpoint, String schema) {
		String schemaToMatch = MessageFormat.format("schemas/{0}/{1}.json", endpoint, schema);
		response.then().assertThat().body(matchesJsonSchemaInClasspath(schemaToMatch));
	}
}