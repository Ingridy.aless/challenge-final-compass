package datafactory;

import static constants.DataFaker.*;

import model.Simulacao;

public class DynamicFactory {
	
	public final static Simulacao randomSimulacao() {
        Simulacao simulacao = new Simulacao(nome, cpf, email, valor, parcela, seguro);
        return simulacao;
    }
	
	public final static Simulacao randomSimulacaoChooseValues(Number valor, Integer parcela) {
        Simulacao simulacao = new Simulacao(nome, cpf, email, valor, parcela, seguro);
        return simulacao;
    }
	
	public final static Simulacao randomSimulacaoInvalidEmail() {
        Simulacao simulacao = new Simulacao(nome, cpf, emailInvalido, valor, parcela, seguro);
        return simulacao;
    }
	
	public final static Simulacao randomSimulacaoNull() {
        Simulacao simulacao = new Simulacao(null, null, null, null, null, null);
        return simulacao;
    }
	
	public final static Simulacao randomSimulacaoAltered(String cpf) {
        Simulacao simulacao = new Simulacao(nomeAlterado, cpf, emailAlterado, valorAlterado, parcelaAlterado, seguro);
        return simulacao;
    }
}