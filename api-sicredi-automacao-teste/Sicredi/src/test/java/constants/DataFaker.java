package constants;

import java.util.Locale;
import com.github.javafaker.Faker;

public class DataFaker {

	public static Faker faker = new Faker(Locale.of("pt", "BR"));
	
	public static String nome = faker.name().name();
	public static String cpf = faker.number().digits(11).toString();
	public static String email = faker.internet().emailAddress();
	public static Number valor = faker.number().numberBetween(1000, 40000);
	public static Integer parcela = faker.number().numberBetween(2, 48);
	public static boolean seguro = faker.bool().bool();
	
	public static String cpfComRestricao = "58063164083";
	public static String cpfSemCadastro = faker.number().digits(11).toString();
	
	public static String emailInvalido = faker.name().username() + ("@org");
	public static Integer idInvalido = faker.number().numberBetween(100, 800);
	
	public static Number valorMaior40000 = faker.number().numberBetween(41000, 90000);
	public static Number valorMenor1000 = faker.number().numberBetween(1, 999);
	public static Integer parcelaMenor1 = faker.number().numberBetween(0, 1);
	public static Integer parcelaMaior48 = faker.number().numberBetween(49, 99);
	
	public static String nomeAlterado = faker.name().name();
	public static String emailAlterado = faker.internet().emailAddress();
	public static Number valorAlterado = (float) faker.number().numberBetween(1000, 40000);
	public static Integer parcelaAlterado = faker.number().numberBetween(2, 48);
}