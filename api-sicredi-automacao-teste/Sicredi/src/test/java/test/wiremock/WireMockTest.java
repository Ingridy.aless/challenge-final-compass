package test.wiremock;

import static datafactory.DynamicFactory.*;
import static org.hamcrest.Matchers.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.RegisterExtension;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;

import io.qameta.allure.*;
import io.restassured.response.Response;
import services.BaseRest;

@Epic("Testes utilizando WireMock")
public class WireMockTest {

	@RegisterExtension
	static WireMockExtension wiremock = WireMockExtension.newInstance()
		.options(wireMockConfig()
				.port(9999)
				.extensions(new ResponseTemplateTransformer(true)))
		.build();
	
	@DisplayName("Não deve cadastrar uma simulação com CPF inválido")
	@Description("Não deve cadastrar uma simulação com CPF com letras e caracteres especiais")
	@Test
	public void naoDeveCriarSimulacaoComCpfFormatoInvalidoWireMock() {
		Response response = BaseRest.post("http://localhost:9999/simulacoes", randomSimulacaoAltered("@123AB*54a2"));
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.cpf", is("O CPF deve conter somente números"));
	}
	
	@DisplayName("Não deve consultar um CPF com formato inválido")
	@Description("Não deve consultar se um CPF possui restrição ao digitar um CPF com números de dígitos diferente de 11")
	@Test
	public void naoDeveConsultarCpfComFormatoInvalidoWireMock() {
		Response response = BaseRest.get("http://localhost:9999/restricoes", "1234");
		response.then().assertThat().statusCode(404);
		response.then().assertThat().body("mensagem", is("O CPF deve conter 11 dígitos"));
	}
}