package test.simulacoes;

import static constants.DataFaker.*;
import static constants.Endpoints.*;
import static datafactory.DynamicFactory.*;
import static helper.ServiceHelper.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;

import io.qameta.allure.*;
import io.restassured.response.Response;
import helper.BaseTest;
import services.BaseRest;

@Tag("SIMULACOES")
@Tag("GET")
@Epic("Testes no Endpoint /simulacoes utilizando o verbo GET")
public class GetSimulacoesTest extends BaseTest {
	
	private Integer id;
	
	@BeforeEach
	public void cadastrarSimulacao() {
		Response responsePost = BaseRest.post(SIMULACOES, randomSimulacao());
		id = responsePost.then().extract().path("id");
	}
	
	@AfterEach
	public void excluirSimulacao() {
		BaseRest.delete(SIMULACOES, id);
	}
	
	@DisplayName("Deve listar todas as simulações cadastradas")
	@Severity(SeverityLevel.BLOCKER)
	@Link(name = "[API] Simulações - CT01", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-13")
	@Test
	public void deveListarTodasSimulacoesCadastradas() {
		Response response = BaseRest.get(SIMULACOES, "");
		response.then().assertThat().statusCode(200);
		response.then().assertThat().body("$", (hasSize(3)));
		response.then().assertThat().body("id", allOf(hasSize(3), is(notNullValue())));
		response.then().assertThat().body("nome", allOf(hasSize(3), is(notNullValue())));
		response.then().assertThat().body("cpf", allOf(hasSize(3), is(notNullValue())));
		response.then().assertThat().body("email", allOf(hasSize(3), is(notNullValue())));
		response.then().assertThat().body("valor", allOf(hasSize(3), is(notNullValue())));
		response.then().assertThat().body("parcelas", allOf(hasSize(3), is(notNullValue())));
		response.then().assertThat().body("seguro", allOf(hasSize(3), is(notNullValue())));
		validarJsonSchema(response, SIMULACOES, "get/200_All");
	}
	
	@DisplayName("Deve listar uma simulação cadastrada pelo CPF")
	@Severity(SeverityLevel.BLOCKER)
	@Link(name = "[API] Simulações - CT02", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-14")
	@Test
	public void deveListarSimulacaoCadastrada() {
		Response response = BaseRest.get(SIMULACOES, cpf);
		response.then().assertThat().statusCode(200);
		response.then().assertThat().body("id", is(notNullValue()));
		response.then().assertThat().body("nome", is(notNullValue()));
		response.then().assertThat().body("cpf", is(notNullValue()));
		response.then().assertThat().body("email", is(notNullValue()));
		response.then().assertThat().body("valor", is(notNullValue()));
		response.then().assertThat().body("parcelas", is(notNullValue()));
		response.then().assertThat().body("seguro", is(notNullValue()));
		response.then().assertThat().body("cpf", hasLength(11));
		response.then().assertThat().body("email", is(matchesRegex("^\\S+@\\S+\\.\\S+$")));
		response.then().assertThat().body("valor", allOf(greaterThan(999f), lessThan(41000f)));
		response.then().assertThat().body("parcelas", allOf(greaterThan(1), lessThan(49)));
		response.then().assertThat().body("seguro", anyOf(is(true), is(false)));
		validarJsonSchema(response, SIMULACOES, "get/200_Cpf");
	}
	
	@DisplayName("Não deve listar uma simulação não cadastrada pelo CPF")
	@Severity(SeverityLevel.NORMAL)
	@Link(name = "[API] Simulações - CT03", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-15")
	@Test
	public void naoDeveListarSimulacaoNaoCadastrada() {
		Response response = BaseRest.get(SIMULACOES, cpfSemCadastro);
		response.then().assertThat().statusCode(404);
		response.then().assertThat().body("mensagem", is("CPF " + cpfSemCadastro +" não encontrado"));
		validarJsonSchema(response, SIMULACOES, "get/404");
	}
}