package test.simulacoes;

import static constants.DataFaker.*;
import static constants.Endpoints.*;
import static datafactory.DynamicFactory.*;
import static helper.ServiceHelper.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;

import io.qameta.allure.*;
import io.restassured.response.Response;
import helper.BaseTest;
import services.BaseRest;

@Tag("SIMULACOES")
@Tag("PUT")
@Epic("Testes no Endpoint /simulacoes utilizando o verbo PUT")
public class PutSimulacoesTest extends BaseTest {

	private Integer id;
	private String cpf;
	
	@BeforeEach
	public void cadastrarSimulacao() {
		Response responsePost = BaseRest.post(SIMULACOES, randomSimulacao());
		id = responsePost.then().extract().path("id");
		cpf = responsePost.then().extract().path("cpf");
	}
	
	@AfterEach
	public void excluirSimulacao() {
		if(id != null) { BaseRest.delete(SIMULACOES, id); }
	}
	
	@DisplayName("Deve alterar uma simulação pelo CPF")
	@Severity(SeverityLevel.BLOCKER)
	@Link(name = "[API] Simulações - CT20", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-28")
	@Test
	public void deveAlterarSimulacaoPeloCpf() {
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoAltered(cpfSemCadastro));
		response.then().assertThat().statusCode(200);
		response.then().assertThat().body("id", is(notNullValue()));;
		response.then().assertThat().body("nome", is(notNullValue()));
		response.then().assertThat().body("cpf", is(notNullValue()));
		response.then().assertThat().body("email", is(notNullValue()));
		response.then().assertThat().body("valor", is(notNullValue()));
		response.then().assertThat().body("parcelas", is(notNullValue()));
		response.then().assertThat().body("seguro", is(notNullValue()));
		response.then().assertThat().body("cpf", hasLength(11));
		response.then().assertThat().body("email", is(matchesRegex("^\\S+@\\S+\\.\\S+$")));
		response.then().assertThat().body("valor", allOf(greaterThan(999f), lessThan(41000f)));
		response.then().assertThat().body("parcelas", allOf(greaterThan(1), lessThan(49)));
		response.then().assertThat().body("seguro", anyOf(is(true), is(false)));
		validarJsonSchema(response, SIMULACOES, "put/200");
	}
	
	@DisplayName("Não deve alterar uma simulação não cadastrada")
	@Severity(SeverityLevel.CRITICAL)
	@Link(name = "[API] Simulações - CT21", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-29")
	@Test
	public void naoDeveAlterarSimulacaoNaoCadastrada() {
		Response response = BaseRest.put(SIMULACOES, cpfSemCadastro, randomSimulacao());
		response.then().assertThat().statusCode(404);
		response.then().assertThat().body("mensagem", is("CPF " + cpfSemCadastro +" não encontrado"));
		validarJsonSchema(response, SIMULACOES, "put/404");
	}
	
	@DisplayName("Não deve alterar uma simulação com CPF já cadastrado")
	@Severity(SeverityLevel.CRITICAL)
	@Link(name = "[API] Simulações - CT22", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-30")
	@Test
	public void naoDeveAlterarSimulacaoComCpfDuplicado() {
		Response responsePost = BaseRest.post(SIMULACOES, randomSimulacaoAltered(cpfSemCadastro));
		Integer idCpfDuplicado = responsePost.then().extract().path("id");
		String cpfDuplicado = responsePost.then().extract().path("cpf");
		
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoAltered(cpfDuplicado));
		response.then().assertThat().statusCode(400);
		validarJsonSchema(response, SIMULACOES, "put/400_cpf");
		
		BaseRest.delete(SIMULACOES, idCpfDuplicado);
	}
	
	@DisplayName("Não deve alterar uma simulação com e-mail inválido")
	@Severity(SeverityLevel.NORMAL)
	@Link(name = "[API] Simulações - CT25", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-33")
	@Test
	public void naoDeveAlterarSimulacaoComEmailInvalido() {
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoInvalidEmail());	
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.email", (is("E-mail deve ser um e-mail válido")));
		validarJsonSchema(response, SIMULACOES, "put/400_email");
	}
	
	@Disabled
	@Tag("BUG")
	@DisplayName("Não deve alterar uma simulação com valor menor que R$ 1.000,00")
	@Severity(SeverityLevel.MINOR)
	@Link(name = "[API] Simulações - CT26", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-34")
	@Test
	public void naoDeveAlterarSimulacaoParaValorMenorQue1000() {
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoChooseValues(valorMenor1000, parcela));
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.valor", (is("Valor deve ser maior ou igual a R$ 1.000")));
		validarJsonSchema(response, SIMULACOES, "put/400_valor");
	}
	
	@Disabled
	@Tag("BUG")
	@DisplayName("Não deve alterar uma simulação com valor maior que R$ 40.000,00")
	@Severity(SeverityLevel.MINOR)
	@Link(name = "[API] Simulações - CT27", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-35")
	@Test
	public void naoDeveAlterarSimulacaoParaValorMaiorQue40000() {
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoChooseValues(valorMaior40000, parcela));
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.valor", (is("Valor deve ser menor ou igual a R$ 40.000")));
		validarJsonSchema(response, SIMULACOES, "put/400_valor");
	}
	
	@DisplayName("Não deve alterar uma simulação com parcela menor que 2 vezes")
	@Severity(SeverityLevel.TRIVIAL)
	@Link(name = "[API] Simulações - CT28", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-36")
	@Test
	public void naoDeveAlterarSimulacaoParaParcelaMenorQue2() {
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoChooseValues(valor, parcelaMenor1));	
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.parcelas", (is("Parcelas deve ser igual ou maior que 2")));
		validarJsonSchema(response, SIMULACOES, "put/400_parcelas");
	}
	
	@Disabled
	@Tag("BUG")
	@DisplayName("Não deve alterar uma simulação com parcela maior que 48 vezes")
	@Severity(SeverityLevel.TRIVIAL)
	@Link(name = "[API] Simulações - CT29", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-37")
	@Test
	public void naoDeveAlterarSimulacaoParaParcelaMaiorQue48() {
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoChooseValues(valor, parcelaMaior48));
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.parcelas", (is("Parcelas deve ser igual ou menor que 48")));
		validarJsonSchema(response, SIMULACOES, "put/400_parcelas");
	}
	
	@Disabled
	@Tag("BUG")
	@DisplayName("Não deve alterar uma simulação com campos obrigatórios nulos")
	@Severity(SeverityLevel.TRIVIAL)
	@Link(name = "[API] Simulações - CT33", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-90")
	@Test
	public void naoDeveAlterarSimulacaoCamposObrigatoriosNulos() {
		Response response = BaseRest.put(SIMULACOES, cpf, randomSimulacaoNull());
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.nome", (is("Nome não pode ser vazio")));
		response.then().assertThat().body("erros.email", (is("E-mail não deve ser vazio")));
		response.then().assertThat().body("erros.cpf", (is("CPF não pode ser vazio")));
		response.then().assertThat().body("erros.valor", (is("Valor não pode ser vazio")));
		response.then().assertThat().body("erros.parcelas", (is("Parcelas não pode ser vazio")));
		validarJsonSchema(response, SIMULACOES, "put/400_campos_nulos");
	}
}