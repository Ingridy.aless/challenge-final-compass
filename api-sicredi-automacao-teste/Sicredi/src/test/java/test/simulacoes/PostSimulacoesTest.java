package test.simulacoes;

import static constants.DataFaker.*;
import static constants.Endpoints.*;
import static datafactory.DynamicFactory.*;
import static helper.ServiceHelper.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;

import io.qameta.allure.*;
import io.restassured.response.Response;
import helper.BaseTest;
import services.BaseRest;

@Tag("SIMULACOES")
@Tag("POST")
@Epic("Testes no Endpoint /simulacoes utilizando o verbo POST")
public class PostSimulacoesTest extends BaseTest {
	
	private Integer id;
	
	@AfterEach
	public void excluirSimulacao() {
		if(id != null) { BaseRest.delete(SIMULACOES, id); }
	}
	
	@DisplayName("Deve cadastrar uma simulação corretamente")
	@Severity(SeverityLevel.BLOCKER)
	@Link(name = "[API] Simulações - CT04", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-16")
	@Test
	public void deveCadastrarSimulacao() { 
		Response response = BaseRest.post(SIMULACOES, randomSimulacao());
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(201);
		response.then().assertThat().body("id", is(notNullValue()));;
		response.then().assertThat().body("nome", is(notNullValue()));;
		response.then().assertThat().body("cpf", is(notNullValue()));;
		response.then().assertThat().body("email", is(notNullValue()));;
		response.then().assertThat().body("valor", is(notNullValue()));;
		response.then().assertThat().body("parcelas", is(notNullValue()));;
		response.then().assertThat().body("seguro", is(notNullValue()));;
		response.then().assertThat().body("cpf", hasLength(11));
		response.then().assertThat().body("email", is(matchesRegex("^\\S+@\\S+\\.\\S+$")));
		response.then().assertThat().body("valor", allOf(greaterThan(999), lessThan(41000)));
		response.then().assertThat().body("parcelas", allOf(greaterThan(1), lessThan(49)));
		response.then().assertThat().body("seguro", anyOf(is(true), is(false)));
		validarJsonSchema(response, SIMULACOES, "post/201");
	}

	@DisplayName("Não deve cadastrar uma simulação com CPF já cadastrado")
	@Severity(SeverityLevel.CRITICAL)
	@Link(name = "[API] Simulações - CT05", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-17")
	@Test
	public void naoDeveCadastrarSimulacaoComCpfDuplicado() {
		Response response = BaseRest.post(SIMULACOES, randomSimulacao());
		id = response.then().extract().path("id");		
		
		Response responseCpfDuplicado = BaseRest.post(SIMULACOES, randomSimulacao());
		responseCpfDuplicado.then().assertThat().statusCode(400);
		validarJsonSchema(responseCpfDuplicado, SIMULACOES, "post/400_cpf");
	}
	
	@DisplayName("Não deve cadastrar uma simulação com e-mail inválido")
	@Severity(SeverityLevel.NORMAL)
	@Link(name = "[API] Simulações - CT08", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-20")
	@Test
	public void naoDeveCadastrarSimulacaoComEmailInvalido() {
		Response response = BaseRest.post(SIMULACOES, randomSimulacaoInvalidEmail());
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.email", (is("E-mail deve ser um e-mail válido")));
		validarJsonSchema(response, SIMULACOES, "post/400_email");
	}
	
	@Disabled
	@Tag("BUG")
	@DisplayName("Não deve cadastrar uma simulação com valor menor que R$ 1.000,00")
	@Severity(SeverityLevel.MINOR)
	@Link(name = "[API] Simulações - CT09", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-21")
	@Test
	public void naoDeveCadastrarSimulacaoParaValorMenorQue1000() {
		Response response = BaseRest.post(SIMULACOES, randomSimulacaoChooseValues(valorMenor1000, parcela));
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.valor", (is("Valor deve ser maior ou igual a R$ 1.000")));
		validarJsonSchema(response, SIMULACOES, "post/400_valor");
	}
	
	@DisplayName("Não deve cadastrar uma simulação com valor maior que R$ 40.000,00")
	@Severity(SeverityLevel.MINOR)
	@Link(name = "[API] Simulações - CT10", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-22")
	@Test
	public void naoDeveCadastrarSimulacaoParaValorMaiorQue40000() {
		Response response = BaseRest.post(SIMULACOES, randomSimulacaoChooseValues(valorMaior40000, parcela));
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.valor", (is("Valor deve ser menor ou igual a R$ 40.000")));
		validarJsonSchema(response, SIMULACOES, "post/400_valor");
	}
	
	@DisplayName("Não deve cadastrar uma simulação com parcela menor que 2 vezes")
	@Severity(SeverityLevel.TRIVIAL)
	@Link(name = "[API] Simulações - CT11", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-23")
	@Test
	public void naoDeveCadastrarSimulacaoParaParcelaMenorQue2() {
		Response response = BaseRest.post(SIMULACOES, randomSimulacaoChooseValues(valor, parcelaMenor1));
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.parcelas", (is("Parcelas deve ser igual ou maior que 2")));
		validarJsonSchema(response, SIMULACOES, "post/400_parcelas");
	}
	
	@Disabled
	@Tag("BUG")
	@DisplayName("Não deve cadastrar uma simulação com parcela maior que 48 vezes")
	@Severity(SeverityLevel.TRIVIAL)
	@Link(name = "[API] Simulações - CT12", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-24")
	@Test
	public void naoDeveCadastrarSimulacaoParaParcelaMaiorQue48() {
		Response response = BaseRest.post(SIMULACOES, randomSimulacaoChooseValues(valor, parcelaMaior48));
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.parcelas", (is("Parcelas deve ser igual ou menor que 48")));
		validarJsonSchema(response, SIMULACOES, "post/400_parcelas");
	}
	
	@DisplayName("Não deve cadastrar uma simulação com campos obrigatórios nulos")
	@Severity(SeverityLevel.TRIVIAL)
	@Link(name = "[API] Simulações - CT16", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-81")
	@Test
	public void naoDeveCadastrarSimulacaoCamposObrigatoriosNulos() {
		Response response = BaseRest.post(SIMULACOES, randomSimulacaoNull());
		id = response.then().extract().path("id");		
		response.then().assertThat().statusCode(400);
		response.then().assertThat().body("erros.nome", (is("Nome não pode ser vazio")));
		response.then().assertThat().body("erros.email", (is("E-mail não deve ser vazio")));
		response.then().assertThat().body("erros.cpf", (is("CPF não pode ser vazio")));
		response.then().assertThat().body("erros.valor", (is("Valor não pode ser vazio")));
		response.then().assertThat().body("erros.parcelas", (is("Parcelas não pode ser vazio")));
		validarJsonSchema(response, SIMULACOES, "post/400_campos_nulos");
	}
}