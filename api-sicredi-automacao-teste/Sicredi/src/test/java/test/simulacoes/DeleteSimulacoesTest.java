package test.simulacoes;

import static constants.DataFaker.*;
import static constants.Endpoints.*;
import static datafactory.DynamicFactory.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;

import io.qameta.allure.*;
import io.restassured.response.Response;
import helper.BaseTest;
import services.BaseRest;

@Tag("SIMULACOES")
@Tag("DELETE")
@Epic("Testes no Endpoint /simulacoes utilizando o verbo DELETE")
public class DeleteSimulacoesTest extends BaseTest {
	
	@DisplayName("Deve deletar uma simulação pelo seu Id")
	@Severity(SeverityLevel.BLOCKER)
	@Link(name = "[API] Simulações - CT18", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-26")
	@Test
	public void deveDeletarSimulacaoPeloId() {
		Response responsePost = BaseRest.post(SIMULACOES, randomSimulacao());
		Integer id = responsePost.then().extract().path("id");
		
		Response response = BaseRest.delete(SIMULACOES, id);
		response.then().assertThat().statusCode(200);
		response.then().assertThat().body(hasLength(2));
		response.then().assertThat().body(containsString("OK"));
	}
	
	@DisplayName("Não deve deletar uma simulação não cadastrada")
	@Severity(SeverityLevel.MINOR)
	@Link(name = "[API] Simulações - CT19", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-27")
	@Test
	public void naoDeveDeletarSimulacaoNaoCadastrada() {
		Response response = BaseRest.delete(SIMULACOES, idInvalido);
		response.then().assertThat().statusCode(200);
		response.then().assertThat().body(hasLength(2));
		response.then().assertThat().body(containsString("OK"));
	}
}