package test.fluxos;

import static constants.DataFaker.*;
import static constants.Endpoints.*;
import static datafactory.DynamicFactory.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;

import io.qameta.allure.*;
import io.restassured.response.Response;
import helper.BaseTest;
import services.BaseRest;

@Epic("Testes de fluxo de CRUD de simulação com sucesso")
public class SuccessFlowTest extends BaseTest {
	
	@DisplayName("Deve realizar um fluxo completo de CRUD de Simulação")
	@Description("Testes de fluxo CRUD de simulação com sucesso, consultando um CPF sem restrição")
	@Test
	public void deveRealizarFluxoCompletoDeSimulacao() {
		// Consultar CPF sem restrição 
		Response responseGetRestricoes = BaseRest.get(RESTRICOES, cpf);
		responseGetRestricoes.then().assertThat().statusCode(204);
		responseGetRestricoes.then().assertThat().body(is(emptyOrNullString()));
		
		// Cadastrar simulação
		Response responsePostSimulacao = BaseRest.post(SIMULACOES, randomSimulacao());
		Integer id = responsePostSimulacao.then().extract().path("id");		
		responsePostSimulacao.then().assertThat().statusCode(201);
		responsePostSimulacao.then().assertThat().body(is(notNullValue()));
		
		// Listar simulação pelo CPF
		Response responseGetSimulacao = BaseRest.get(SIMULACOES, cpf);
		responseGetSimulacao.then().assertThat().statusCode(200);
		responseGetSimulacao.then().assertThat().body(is(notNullValue()));
		
		// Alterar simulação pelo CPF
		Response responsePutSimulacao = BaseRest.put(SIMULACOES, cpf, randomSimulacaoAltered(cpfSemCadastro));
		responsePutSimulacao.then().assertThat().statusCode(200);
		responsePutSimulacao.then().assertThat().body(is(notNullValue()));
		
		// Deletar simulação pelo ID
		Response responseDeleteSimulacao = BaseRest.delete(SIMULACOES, id);
		responseDeleteSimulacao.then().assertThat().statusCode(200);
		responseDeleteSimulacao.then().assertThat().body(containsString("OK"));
	}
}