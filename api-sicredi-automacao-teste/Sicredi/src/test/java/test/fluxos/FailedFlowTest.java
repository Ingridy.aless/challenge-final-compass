package test.fluxos;

import static constants.DataFaker.*;
import static constants.Endpoints.*;
import static datafactory.DynamicFactory.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;

import io.qameta.allure.*;
import io.restassured.response.Response;
import helper.BaseTest;
import services.BaseRest;

@Epic("Testes de fluxo de CRUD de simulação com falha")
public class FailedFlowTest extends BaseTest {
	
	@DisplayName("Não deve realizar um fluxo completo de CRUD de Simulação")
	@Description("Testes de fluxo CRUD de simulação com falha, consultando um CPF com restrição")
	@Test
	public void naoDeveRealizarFluxoCompletoDeSimulacao() {
		// Consultar CPF com restrição 
		Response responseGetRestricoes = BaseRest.get(RESTRICOES, cpfComRestricao);
		responseGetRestricoes.then().assertThat().statusCode(200);
		responseGetRestricoes.then().assertThat().body("mensagem", is("O CPF " + cpfComRestricao +" tem problema"));
		
		// Não deve cadastrar simulação com e-mail inválido
		Response responsePostSimulacao = BaseRest.post(SIMULACOES, randomSimulacaoInvalidEmail());
		Integer id = responsePostSimulacao.then().extract().path("id");		
		responsePostSimulacao.then().assertThat().statusCode(400);
		responsePostSimulacao.then().assertThat().body("erros.email", (is("E-mail deve ser um e-mail válido")));
		
		// Não deve listar simulação não cadastrada pelo CPF
		Response responseGetSimulacao = BaseRest.get(SIMULACOES, cpf);
		responseGetSimulacao.then().assertThat().statusCode(404);
		responseGetSimulacao.then().assertThat().body("mensagem", is("CPF " + cpf +" não encontrado"));
		
		// Não deve alterar simulação não cadastrada pelo CPF
		Response responsePutSimulacao = BaseRest.put(SIMULACOES, cpf, randomSimulacaoAltered(cpfSemCadastro));
		responsePutSimulacao.then().assertThat().statusCode(404);
		responsePutSimulacao.then().assertThat().body("mensagem", is("CPF " + cpf +" não encontrado"));
		
		// Não deve deletar simulação não cadastrada pelo CPF
		Response responseDeleteSimulacao = BaseRest.delete(SIMULACOES, id);
		responseDeleteSimulacao.then().assertThat().statusCode(400);
	}
}