package test.restricoes;

import static constants.DataFaker.*;
import static constants.Endpoints.*;
import static helper.ServiceHelper.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;

import io.qameta.allure.*;
import io.restassured.response.Response;
import helper.BaseTest;
import services.BaseRest;

@Tag("RESTRICOES")
@Tag("GET")
@Epic("Testes no Endpoint /restricoes utilizando o verbo GET")
public class GetRestricoesTest extends BaseTest {
	
	@DisplayName("Deve consultar um CPF sem restrição")
	@Severity(SeverityLevel.CRITICAL)
	@Link(name = "[API] Restrições - CT01", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-11")
	@Test
	public void deveConsultarCpfSemRestricao() {
		Response response = BaseRest.get(RESTRICOES, cpf);
		response.then().assertThat().statusCode(204);
		response.then().assertThat().body(is(emptyOrNullString()));
	}
	
	@DisplayName("Deve consultar um CPF com restrição")
	@Severity(SeverityLevel.BLOCKER)
	@Link(name = "[API] Restrições - CT02", url = "https://ingridyalessandretti7.atlassian.net/browse/AS-12")
	@Test
	public void deveConsultarCpfComRestricao() {
		Response response = BaseRest.get(RESTRICOES, cpfComRestricao);
		response.then().assertThat().statusCode(200);
		response.then().assertThat().body("mensagem", is("O CPF " + cpfComRestricao +" tem problema"));
		validarJsonSchema(response, RESTRICOES, "get/200");
	}
}